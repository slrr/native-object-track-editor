﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using System.Windows.Media.Animation;
using System.Runtime.InteropServices;
using SlrrLib.View;

namespace NativeObjectTrackEditor
{
  public partial class MainWindow : Window
  {
    private SlrrLib.Model.DynamicNamedSpatialData currentHighlight = null;
    private SlrrLib.Geom.NativeGeometry nativeGeom = new SlrrLib.Geom.NativeGeometry(null, "");
    private SlrrLib.Geom.MapRpkModelFactory currentFactory;
    private IEnumerable<SlrrLib.Geom.NamedModel> spatialData = null;
    private List<SlrrLib.Model.DynamicNamedSpatialData> inPreview = new List<SlrrLib.Model.DynamicNamedSpatialData>();
    private float tickForMove = 0.01f;
    private bool spatialShown = false;
    private bool selectionLocked = false;

    public MainWindow()
    {
      InitializeComponent();
      if (System.IO.File.Exists("lastDir"))
        ctrlOrbitingViewport.LastRpkDirectory = System.IO.File.ReadAllText("lastDir");
      SlrrLib.Model.MessageLog.SetConsoleLogOutput();
      ctrlOrbitingViewport.IsViewDistanceManaged = true;
      ctrlOrbitingViewport.ObjectViewDistance = 1000;
      ctrlOrbitingViewport.MarkerMoved += ctrlOrbitingViewport_MarkerMoved;
    }

    private void loadSceneRPK()
    {
      currentFactory = ctrlOrbitingViewport.LoadSceneRpk();
      if(currentFactory == null)
        return;
      Title = ctrlOrbitingViewport.LastRpkOpened;
      nativeGeom = new SlrrLib.Geom.NativeGeometry(new SlrrLib.Model.DynamicRpk(currentFactory.RpkLoaded), ctrlOrbitingViewport.LastRpkOpened);
      nativeGeom.GenerateVisuals();
      ctrlTextBoxExtrRefs.TextChanged -= ctrlTextBoxExtrRefs_TextChanged;
      ctrlTextBoxExtrRefs.Text = nativeGeom.Rpk.ExternalReferences.Aggregate((x, y) => x + "\r\n" + y);
      ctrlTextBoxExtrRefs.TextChanged += ctrlTextBoxExtrRefs_TextChanged;
      ctrlComboTypeIDs.Items.Clear();
      ctrlComboRelativeTo.Items.Clear();
      foreach (var item in nativeGeom.AllPossibleObjects())
      {
        ctrlComboTypeIDs.Items.Add(item);
        ctrlComboRelativeTo.Items.Add(item);
      }
      foreach (var model in nativeGeom.DynamicNamedSpatialDataToGeom)
      {
        ctrlOrbitingViewport.AddModelToScene(new SlrrLib.Geom.NamedModel
        {
          ModelGeom = model.Value,
          Name = model.Key.Name,
          Translate = new Vector3D(model.Key.BoundingBoxX, model.Key.BoundingBoxY, model.Key.BoundingBoxZ)
        });
        ctrlOrbitingViewport.UpdateVisibleObjectsViewDistance();
      }
    }
    private void processMarkerPositionTextboxTextChange()
    {
      if (currentHighlight == null)
        return;
      var pos = currentHighlight.GetPositionFromParamsLineFromFirstRSD();
      float tmp = 0;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerX.Text, out tmp))
        pos.X = tmp;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerY.Text, out tmp))
        pos.Y = tmp;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerZ.Text, out tmp))
        pos.Z = tmp;
      ctrlOrbitingViewport.LookAtPosition(pos); // which udpdates the marker and triggers ctrlOrbitingViewport_MarkerMoved
    }
    private void removeSpatialDataStructVisualisation()
    {
      if (spatialData == null)
        return;
      spatialShown = false;
      foreach (var model in spatialData)
        ctrlOrbitingViewport.RemoveModelFromScene(model);
    }
    private void addSpatialDataStructVisualisation()
    {
      if (nativeGeom != null && currentHighlight != null)
      {
        removeSpatialDataStructVisualisation();
        spatialShown = true;
        spatialData = nativeGeom.SpatialRepresentationForObject(currentHighlight).Select(x => new SlrrLib.Geom.NamedModel
        {
          ModelGeom = x,
          Name = "Spatial struct box",
          Translate = new Vector3D(x.Bounds.X, x.Bounds.Y, x.Bounds.Z)
        }).ToList();
        ctrlTextBlockspatial.Text = "Depth: " + nativeGeom.LastSpatialTraceDepth.ToString();
        foreach (var model in spatialData)
          ctrlOrbitingViewport.AddModelToScene(model);
      }
    }
    private float getMouseMoveTick(MouseWheelEventArgs e)
    {
      return Math.Sign(e.Delta) * tickForMove * (Keyboard.IsKeyDown(Key.LeftShift) ? 100.0f : 10.0f);
    }
    private void processMarkerRotationTextboxTextChange()
    {
      if (currentHighlight == null)
        return;
      var pos = currentHighlight.GetPositionFromParamsLineFromFirstRSD();
      var ypr = currentHighlight.GetYawPitchRollFromParamsLineFromFirstRSD();
      float tmp = 0;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerYaw.Text, out tmp))
        ypr.X = tmp;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerRoll.Text, out tmp))
        ypr.Y = tmp;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerPitch.Text, out tmp))
        ypr.Z = tmp;
      nativeGeom.SetNewPositionForData(currentHighlight, pos, ypr);
    }
    private void removeNamedData(SlrrLib.Model.DynamicNamedSpatialData dat)
    {
      ctrlOrbitingViewport.RemoveAllModelsBy3DRepresentation(nativeGeom.DynamicNamedSpatialDataToGeom[dat]);
      nativeGeom.RemoveNamedDataFromRpk(dat);
    }
    private SlrrLib.Model.DynamicNamedSpatialData addNamedData(SlrrLib.Geom.NativeGeometryObjResEntryContext selItem, Vector3D pos, Vector3D rot)
    {
      var addedNative = nativeGeom.AddNamedDataFromDef(selItem, pos, rot);
      var model = nativeGeom.DynamicNamedSpatialDataToGeom[addedNative];
      ctrlOrbitingViewport.AddModelToScene(new SlrrLib.Geom.NamedModel
      {
        ModelGeom = model,
        Name = addedNative.Name,
        Translate = new Vector3D(addedNative.BoundingBoxX, addedNative.BoundingBoxY, addedNative.BoundingBoxZ)
      });
      return addedNative;
    }
    private void clearPreview()
    {
      foreach (var temp in inPreview)
        removeNamedData(temp);
      inPreview.Clear();
    }
    private void massAddNatives(bool toPreview)
    {
      if (toPreview)
        clearPreview();
      if (!(ctrlComboRelativeTo.SelectedItem is SlrrLib.Geom.NativeGeometryObjResEntryContext selItemRelTo))
        return;
      if (!(ctrlComboTypeIDs.SelectedItem is SlrrLib.Geom.NativeGeometryObjResEntryContext selItemToAdd))
        return;
      Vector3D relPos = new Vector3D();
      Vector3D relYpr = new Vector3D();
      try
      {
        relPos.X = UIUtil.ParseOrThrow(ctrlTextBoxRelativeX.Text);
        relPos.Y = UIUtil.ParseOrThrow(ctrlTextBoxRelativeY.Text);
        relPos.Z = UIUtil.ParseOrThrow(ctrlTextBoxRelativeZ.Text);
        relYpr.X = UIUtil.ParseOrThrow(ctrlTextBoxRelativeYaw.Text);
        relYpr.Y = UIUtil.ParseOrThrow(ctrlTextBoxRelativePitch.Text);
        relYpr.Z = UIUtil.ParseOrThrow(ctrlTextBoxRelativeRoll.Text);
      }
      catch (Exception)
      {
        MessageBox.Show("Invalid string in the relative position or rotation");
        return;
      }
      foreach (var namedDat in nativeGeom.DynamicNamedSpatialDataToGeom.Keys.ToList())
      {
        if (namedDat.GetIntHexValueFromLineNameInFirstRSD("gametype") == selItemRelTo.asExternalTypeID)
        {
          var localRotate = namedDat.GetYawPitchRollFromParamsLineFromFirstRSD();
          var ypr = new SlrrLib.Geom.YprRotation3D(localRotate, false)
          {
            yAxis = new Vector3D(0, 1, 0),
            pAxis = new Vector3D(1, 0, 0)
          };
          Vector3D locRelPos = relPos;
          locRelPos = ypr.TransformValues.Transform(locRelPos);
          var newNative = addNamedData(selItemToAdd, locRelPos + namedDat.GetPositionFromParamsLineFromFirstRSD(), relYpr + localRotate);
          if (toPreview)
            inPreview.Add(newNative);
        }
      }
      ctrlOrbitingViewport.UpdateVisibleObjectsViewDistance();
    }

    private void ctrlOrbitingViewport_MarkerMoved(object sender, EventArgs e)
    {
      if (nativeGeom != null && !selectionLocked)
      {
        nativeGeom.AutoResolve = ctrlCheckBoxAutoResolve.IsChecked == true;
        var ClosestObj = nativeGeom.GetClosestEntity(ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace);
        if (ClosestObj != currentHighlight && ClosestObj != null)
        {
          nativeGeom.ClearHighLight();
          nativeGeom.TopHighLightDynamicNamedSpatialData(ClosestObj);
          currentHighlight = ClosestObj;
          int typeID = ClosestObj.GetIntHexValueFromLineNameInFirstRSD("gametype");
          var cfg = nativeGeom.CfgOfTypeID(typeID, nativeGeom.SlrrRoot, nativeGeom.Rpk);
          if (spatialShown)
            ctrlButtonResolveSpatialStruct_Click(null, null);
          if (cfg != null)
          {
            try
            {
              ctrlTextBlockSelectionInfo.Text = nativeGeom.Rpk.ExternalReferences[(typeID >> 16) - 1] + "  0x" + cfg.res.TypeID.ToString("X8") + "  |  " + cfg.res.Alias;
            }
            catch (Exception) { }
          }
          else if (ClosestObj.GetFirstRSDEntry() != null)
          {
            string rpk = "local";
            if ((typeID >> 16) - 1 >= 0)
              rpk = nativeGeom.Rpk.ExternalReferences[(typeID >> 16) - 1];
            ctrlTextBlockSelectionInfo.Text = rpk + "  0x" + typeID.ToString("X8") + "\r\nName: " +
                                              ClosestObj.Name + "\r\nRSD:\r\n" + ClosestObj.GetFirstRSDEntry().StringData;
          }
        }
      }
      else if (nativeGeom != null)
      {
        nativeGeom.SetNewPositionForData(currentHighlight, ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace, currentHighlight.GetYawPitchRollFromParamsLineFromFirstRSD());
      }
      if (currentHighlight != null)
      {
        var posOfObj = currentHighlight.GetPositionFromParamsLineFromFirstRSD();
        var yprOfObj = currentHighlight.GetYawPitchRollFromParamsLineFromFirstRSD();
        ctrlTextBoxMarkerX.Text = posOfObj.X.ToString("F3");
        ctrlTextBoxMarkerY.Text = posOfObj.Y.ToString("F3");
        ctrlTextBoxMarkerZ.Text = posOfObj.Z.ToString("F3");
        ctrlTextBoxMarkerYaw.Text = yprOfObj.X.ToString("F3");
        ctrlTextBoxMarkerPitch.Text = yprOfObj.Y.ToString("F3");
        ctrlTextBoxMarkerRoll.Text = yprOfObj.Z.ToString("F3");
      }
    }
    private void ctrlButtonAddAtPos_Click(object sender, RoutedEventArgs e)
    {
      if (ctrlComboTypeIDs.SelectedItem is SlrrLib.Geom.NativeGeometryObjResEntryContext selItem)
      {
        addNamedData(selItem, ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace, new Vector3D());
        ctrlOrbitingViewport.UpdateVisibleObjectsViewDistance();
      }
    }
    private void ctrlButtonAddToScene_Click(object sender, RoutedEventArgs e)
    {
      ctrlOrbitingViewport.LoadSecondaryRpk();
    }
    private void ctrlButtonDelAtPos_Click(object sender, RoutedEventArgs e)
    {
      if (currentHighlight == null)
        return;
      removeNamedData(currentHighlight);
    }
    private void ctrlButtonLockSelection_Click(object sender, RoutedEventArgs e)
    {
      selectionLocked = !selectionLocked;
      if (selectionLocked)
      {
        ctrlOrbitingViewport.LookAtPosition(currentHighlight.GetPositionFromParamsLineFromFirstRSD());
        selectionLocked = true;
        ctrlButtonLockSelection.Content = "SelectionIsLocked";
        ctrlButtonLockSelection.Background = Brushes.DarkRed;
      }
      else
      {
        selectionLocked = false;
        ctrlButtonLockSelection.Content = "LockSelection";
        ctrlButtonLockSelection.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0xDD, 0xDD, 0xDD));
      }
    }
    private void ctrlButtonRenderScene_Click(object sender, RoutedEventArgs e)
    {
      loadSceneRPK();
    }
    private void ctrlButtonResolveModel_Click(object sender, RoutedEventArgs e)
    {
      nativeGeom.ResolveVisualsOf(currentHighlight);
    }
    private void ctrlTextBoxPosMarker_KeyUp(object sender, KeyEventArgs e)
    {
      processMarkerPositionTextboxTextChange();
    }
    private void ctrlTextBoxRotMarker_KeyUp(object sender, KeyEventArgs e)
    {
      processMarkerRotationTextboxTextChange();
    }
    private void ctrlTextBoxMarkerX_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerX.Text, out float cur))
      {
        cur += getMouseMoveTick(e);
        ctrlTextBoxMarkerX.Text = UIUtil.FloatToString(cur);
        processMarkerPositionTextboxTextChange();
      }
    }
    private void ctrlTextBoxMarkerY_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerY.Text, out float cur))
      {
        cur += getMouseMoveTick(e);
        ctrlTextBoxMarkerY.Text = UIUtil.FloatToString(cur);
        processMarkerPositionTextboxTextChange();
      }
    }
    private void ctrlTextBoxMarkerZ_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerZ.Text, out float cur))
      {
        cur += getMouseMoveTick(e);
        ctrlTextBoxMarkerZ.Text = UIUtil.FloatToString(cur);
        processMarkerPositionTextboxTextChange();
      }
    }
    private void ctrlTextBoxExtrRefs_TextChanged(object sender, TextChangedEventArgs e)
    {
      nativeGeom.Rpk.ExternalReferences.Clear();
      nativeGeom.Rpk.ExternalReferences.AddRange(ctrlTextBoxExtrRefs.Text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries));
      nativeGeom.Rpk.ExternalReferences.RemoveAll(x => !System.IO.File.Exists(nativeGeom.SlrrRoot + "\\" + x));
    }
    private void ctrlTextBoxMarkerYaw_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerYaw.Text, out float cur))
      {
        cur += getMouseMoveTick(e);
        ctrlTextBoxMarkerYaw.Text = UIUtil.FloatToString(cur);
        processMarkerRotationTextboxTextChange();
      }
    }
    private void ctrlTextBoxMarkerRoll_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerRoll.Text, out float cur))
      {
        cur += getMouseMoveTick(e);
        ctrlTextBoxMarkerRoll.Text = UIUtil.FloatToString(cur);
        processMarkerRotationTextboxTextChange();
      }
    }
    private void ctrlTextBoxMarkerPitch_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarkerPitch.Text, out float cur))
      {
        cur += getMouseMoveTick(e);
        ctrlTextBoxMarkerPitch.Text = UIUtil.FloatToString(cur);
        processMarkerRotationTextboxTextChange();
      }
    }
    private void ctrlButtonSaveRpk_Click(object sender, RoutedEventArgs e)
    {
      var dlg = new Microsoft.Win32.SaveFileDialog
      {
        InitialDirectory = ctrlOrbitingViewport.LastRpkDirectory,
        FileName = "",
        DefaultExt = ".rpk",
        Filter = "rpk|*.rpk"
      };
      var result = dlg.ShowDialog();
      if (result == true)
      {
        double margin = 0.2;
        try
        {
          margin = double.Parse(ctrlTextBoxMargin.Text, System.Globalization.CultureInfo.InvariantCulture);
        }
        catch (Exception) { }
        nativeGeom.AddingBoundMargin = margin;
        nativeGeom.SaveRpk(dlg.FileName);
        ctrlOrbitingViewport.UpdateVisibleObjectsViewDistance();
      }
      else
      {
        return;
      }
    }
    private void ctrlButtonResolveSpatialStruct_Click(object sender, RoutedEventArgs e)
    {
      addSpatialDataStructVisualisation();
    }
    private void ctrlButtonDeleteSpatialStruct_Click(object sender, RoutedEventArgs e)
    {
      removeSpatialDataStructVisualisation();
    }
    private void ctrlButtonReloadPossibleObjects_Click(object sender, RoutedEventArgs e)
    {
      ctrlComboTypeIDs.Items.Clear();
      foreach (var item in nativeGeom.AllPossibleObjects())
        ctrlComboTypeIDs.Items.Add(item);
      ctrlComboRelativeTo.Items.Clear();
      foreach (var item in nativeGeom.AllPossibleObjects())
        ctrlComboRelativeTo.Items.Add(item);
    }
    private void ctrlButtonSelectInCombo_Click(object sender, RoutedEventArgs e)
    {
      foreach (var item in ctrlComboTypeIDs.Items)
      {
        if (item is SlrrLib.Geom.NativeGeometryObjResEntryContext obj)
        {
          if (obj.asExternalTypeID == currentHighlight.GetIntHexValueFromLineNameInFirstRSD("gametype"))
          {
            ctrlComboTypeIDs.SelectedItem = item;
            return;
          }
        }
      }
    }
    private void ctrlButtonAddAllToDirty_Click(object sender, RoutedEventArgs e)
    {
      if (currentHighlight == null)
        return;
      nativeGeom.SetTypeIDDirty(currentHighlight.GetIntHexValueFromLineNameInFirstRSD("gametype"));
    }
    private void ctrlButtonMassAddNative_Click(object sender, RoutedEventArgs e)
    {
      massAddNatives(false);
    }
    private void ctrlTextBoxRelativeX_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxRelativeX.Text, out float cur))
      {
        cur += getMouseMoveTick(e);
        ctrlTextBoxRelativeX.Text = UIUtil.FloatToString(cur);
      }
    }
    private void ctrlTextBoxRelativeY_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxRelativeY.Text, out float cur))
      {
        cur += getMouseMoveTick(e);
        ctrlTextBoxRelativeY.Text = UIUtil.FloatToString(cur);
      }
    }
    private void ctrlTextBoxRelativeZ_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxRelativeZ.Text, out float cur))
      {
        cur += getMouseMoveTick(e);
        ctrlTextBoxRelativeZ.Text = UIUtil.FloatToString(cur);
      }
    }
    private void ctrlTextBoxRelativeYaw_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxRelativeYaw.Text, out float cur))
      {
        cur += getMouseMoveTick(e);
        ctrlTextBoxRelativeYaw.Text = UIUtil.FloatToString(cur);
      }
    }
    private void ctrlTextBoxRelativePitch_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxRelativePitch.Text, out float cur))
      {
        cur += getMouseMoveTick(e);
        ctrlTextBoxRelativePitch.Text = UIUtil.FloatToString(cur);
      }
    }
    private void ctrlTextBoxRelativeRoll_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxRelativeRoll.Text, out float cur))
      {
        cur += getMouseMoveTick(e);
        ctrlTextBoxRelativeRoll.Text = UIUtil.FloatToString(cur);
      }
    }
    private void ctrlButtonMassAddPreview_Click(object sender, RoutedEventArgs e)
    {
      massAddNatives(true);
    }
    private void ctrlButtonSelectInAddToCombo_Click(object sender, RoutedEventArgs e)
    {
      foreach (var item in ctrlComboRelativeTo.Items)
      {
        if (item is SlrrLib.Geom.NativeGeometryObjResEntryContext obj)
        {
          if (obj.asExternalTypeID == currentHighlight.GetIntHexValueFromLineNameInFirstRSD("gametype"))
          {
            ctrlComboRelativeTo.SelectedItem = item;
            return;
          }
        }
      }
    }
    private void ctrlButtonClearPreview_Click(object sender, RoutedEventArgs e)
    {
      clearPreview();
      ctrlOrbitingViewport.UpdateVisibleObjectsViewDistance();
    }
  }
}
